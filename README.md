#Tp2-SinBlancos

#1)Nombres de los integrantes del grupo:

	Luis Veliz

	Gian Luca Bellone

	Mauro Rosales

	Federico Cortez

	
#2)Decisiones de diseno tomadas

	- Al no poder poner parametros opcionales tuvimos que crear un metodo 	diferenciado para Legion que cree una instancia de la misma recibiendo 	como parametros 		la 	cantidad de cada tipo de unidad que 	contiene la misma, asi mismo tambien dentro de esta clase Legion 	debimos hacer un metodo para crear otra Legion 		dentro de 	ella 	"legionDeLegion".
	- Decidimos crear una clase dado para que cada jugador contara con un 	dado propio ( la implementacion de esta clase fue usando el patron 	singleton para que 		solo se pueda contar con una sola instancia de 	la misma).
	- Muchos de los metodos de entrada de datos como lectores, scanners, 	etc no se cierrran porque al hacerlo provocaban un mal funcionamiento 	del menu.
	- Se uso un menu por consola para poder ejecutar la "App".
	- Algunos atributos se marcan no usados cuando en realidad si se usan.
		Se utilizo un plugin para hacer el UML.
	-A la hora de agregar unidades a los ejercitos pensamos en un metodo el 	cual fuese mas facil agregar de a mucha cantidad, para ello cada 	soldado tiene como atributo "cantidadDeSoldados" el cual es modificado 	a traves del agregarSoldados() dicho metodo recibe como parametro el 	tipo de soldado que se quiere agregar y la cantidad
	
	
#3)Paquetes dentro de la carpeta src/main/java

#Excepciones
	Clase ErrorLegionNoExiste
*Lanza excepciones si la legion elegida no existe*

#Juego
	app.java
*Ejecuta el proyecto de todo el trabajo realizado*
Buggs:
*Por tema de java , el double se calcula con muchos decimales que no se pueden controlar, por ende el rango de precision varia entre 0.1*
	
	BatallaDeEjercitos.java
	
Clase la cual va a transcurrir el juego, contiene a los dos jugadores y funciona como intermediaria para transferir el da�o de uno a otro, ademas a traves de ella se puede ver el menu de compra y las legiones predefinidas.
Define al primer jugador a partir de tirar los dados y comparar al mayor.
Por cada vez que se ataque se ejecutara un metodo hayGanador() y ganador() el primero consiste en preguntar si la vida de alguno de los jugadores llego a 0, en dicho caso hay un ganador y a traves de ganador() lo devuelve.
	
	BattleGame.java
*Se crea un menu por cosola*


	Dado.java 
*Crea un dado singleton con numeros al azar*
	
	Jugador.java
	
Clase que contiene una legion a traves del metodo agregarSoldaos() puede agregar soldados a su legion de la manera ya aclarada en las decisiones de dise�o, a la hora de agregar la cantidad que se quiera se multiplica la cantidad por el costo del tipo del soldado y se le resta al dinero (inicializado en 500000 por defecto) del jugador a traves del metodo calcularYMostrarDinero.
Cada jugador contiene un dado.
La vida de los jugadores es la vida de su legion, cuando la vida de esta se vea alterada la del jugador tambien.

#UnidadesDeGuerra

	Ejercito.java
	
Clase abstracta de la cual se instancian sus clases hijas "Soldado.java"; "Legion.java". Dicha clase les proporciona los siguientes atributos:
vida(double);dano(double);danoRecibidoRestante(double);cantidadDeSoldados(double);danoRecibido(double);nombre(String);contadorDeFallos(double).
Todos estos atributos llevan sus getters y setters y seran redefinidos en sus respectivas clases hijas, 

	Soldado.java

Clase abstracta que se extiende de Ejercito.java de La cual se instancias sus clases hijas "Auxiliares.java"; "Legionarios.java"; "Centuriones.java";.
Dicha clase les proporciona los siguientes atributos:
arma(String);costo(double);orden(int).

	Legion.java

Clase que se extiende de Ejercito.java
Su atributo vida es la sumatoria de la vida de todos los soldados que contiene.
Su atributo dano es la sumatoria del dano de todos los soldados que contiene, ademas dicho atributo es aumentado en 10%.por cada centurion que contenga.
Lleva un metodo "agregarLegion()" el cual consiste en crear otra legion que va a estar situada dentro de la legion que ejecute dicho metodo.
El metodo "recibirDano()" consiste en crear un iterador sobre el array de soldadosSueltos provocando da�o respectivamente su orden.

	Auxiliares.java
	
Clase que se extiende de Soldado la cual redefine sus atributos y ademas agrega una particularidad de la clase que consiste en la probabilidad de fallar el 50% de sus ataques.
	
	Legionarios.java
	
Clase que se extiende de Soldado la cual redefine sus atributos, la particularidad de este tipo de soldado es el no fallar ataques por lo cual no se cambio nada.

	Centuriones.java
	
Clase que se extiende de Soldado la cual redefine sus atributos, este tipo de soldado puede esquivar ataques con una probabilidad del 50%, ademas proporciona un potenciador de dano del 10% al ejercito  por cada uno de los centuriones que contenga.


#Test 

	TesDeBatallaDeEjercito.java
	TestJugador.java
	TestLegion.java
	TestSoldado.java

*Se ejecutan todos los test que fueron necesarios para que el proyecto funcione*

#4)Conclusion

A diferencia del primer trabajo practico , el uso intensivo de gitlab  nos ayudo a poder progresar con el segundo trabajo practico facilitandonos al momento de correciones , tiempo y comodidad , ademas se avanzo mucho mas rapido que el trabajo anterior.
Con la experiencia que tuvimos del primer trabajo practico , en este se pudo trabajar con mayor comodida implementando los temas ya aprendidos y agregando nuevas herramientas para poder mejorar el codigo, pudiendo asi mejorar el estilo de programar. 
Para finalizar el trabajo nos ayudo a reforzar los temas estudiados en clase y poder aplicarlos en un trabajo.




		