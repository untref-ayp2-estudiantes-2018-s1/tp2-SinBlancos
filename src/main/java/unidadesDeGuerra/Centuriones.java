package unidadesDeGuerra;

public class Centuriones extends Soldado {
	
	double danoRecibido = 0;
	double cantidadDeCenturiones = 1;
	public Centuriones(int cantidadDeCenturiones) {
		super(cantidadDeCenturiones);
		costo = 200;
		orden = 3;
		this.cantidadDeCenturiones += cantidadDeCenturiones;
		nombre = "Centuriones";
		arma = "Espada corta y Escudo";
	}

	@Override
	public void recibirDano(double danoRecibido) {
		int contadorDeNoEsquivados = 0;
		for (int i = 0; i < cantidadDeCenturiones; i++) {
			if (Math.random() < 0.50) {
				contadorDeNoEsquivados++;
			}
		}
		danoRecibido -= contadorDeNoEsquivados;
		this.danoRecibido = danoRecibido;
		double cantidadDeMuertos = this.danoRecibido / 100;
		cantidadDeCenturiones -= cantidadDeMuertos ;
		
		if (vida < 0) {
			danoRecibidoRestante -= vida;
			vida = 0;
		}

	}
	@Override
	public double getVida() {
		vida = 100*cantidadDeCenturiones;
		return vida;
	}
	@Override
	public double getDano() {
		
		dano = cantidadDeCenturiones-1;
		
		return dano;
	}
	@Override
	public double getDanoRecibido() {
		return danoRecibido;
	}
	@Override
	public void agregarUnidades(int cantidad) {
		cantidadDeCenturiones+= cantidad;
	}

	@Override
	public double getCantidad() {
		return cantidadDeCenturiones;
	}


}
