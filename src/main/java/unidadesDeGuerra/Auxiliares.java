package unidadesDeGuerra;

public class Auxiliares extends Soldado {

	private boolean turnoInicial = true;
	private boolean acierto;
	private double cantidadDeAuxiliares;
	


	public Auxiliares(int cantidadDeAuxiliares) {
		super(cantidadDeAuxiliares);
		costo = 50;
		orden = 1;
		this.cantidadDeAuxiliares += cantidadDeAuxiliares;
		nombre = "Auxiliares";
		arma = "Jabalinas";
	}

	@Override
	public double getDano() {// habilidad de pegar 1 de cada 2 ataques
		double resultado = 0;
		int contadorDeAcertados = 0;
		if(turnoInicial == true) {
			acierto = true;
		}	
		for(int i = 0; i<cantidadDeSoldados-1;i++) {
			
			if(Math.random() > 0.5) {
				acierto = true;
				contadorDeAcertados++;
			}
		}
		if(!acierto){
			resultado = 0;
		}		
		if (acierto) {
			turnoInicial = false;
			resultado = (contadorDeAcertados) * 0.7;
			acierto = false;
		} 
		contadorDeFallos = cantidadDeSoldados-contadorDeAcertados;
		return resultado;
		}
	@Override
	public void recibirDano(double dano) {
		double cantidadDeMuertos  = dano / 100;
		cantidadDeAuxiliares -= cantidadDeMuertos;
		vida -= dano;
		if (vida < 0) {
			danoRecibidoRestante = danoRecibidoRestante - vida;
			vida = 0;
		}
		danoRecibido = dano;
}

	@Override
	public void agregarUnidades(int cantidad) {
		cantidadDeAuxiliares+= cantidad;
	}
	@Override
	public double getVida() {
		vida = 100*cantidadDeAuxiliares;
		return vida;
	}
	@Override
	public double getDanoRecibidoRestante() {
		return danoRecibidoRestante;
	}

	@Override
	public double getDanoRecibido() {
		return danoRecibido;
	}
	
}


