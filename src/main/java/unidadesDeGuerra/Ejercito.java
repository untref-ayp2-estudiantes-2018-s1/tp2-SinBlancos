package unidadesDeGuerra;

import java.util.ArrayList;

public abstract class Ejercito {

	protected double vida = 100;
	protected double dano;
	protected double danoRecibidoRestante = 0;
	protected double cantidadDeSoldados = 1;
	protected double danoRecibido;
	protected int costo;
	protected int orden;
	private ArrayList<Ejercito> soldadosSueltos = new ArrayList<Ejercito>();
	protected String nombre;
	protected double contadorDeFallos;

	public Ejercito(int cantidadDeSoldados) {
		this.cantidadDeSoldados += cantidadDeSoldados;

	}

	public double getDano() {
		return dano;
	}
	public String getNombre() {
		return nombre;
	}
	public double getFallos() {
		return contadorDeFallos ;
	}

	public void recibirDano(double dano) {

		double cantidadDeMuertos = dano / 100;

		cantidadDeSoldados -= cantidadDeMuertos;
		vida -= dano;
		if (vida <= 0) {
			danoRecibidoRestante = danoRecibidoRestante - vida;
			vida = 0;
		} else {
			danoRecibidoRestante = 0;
		}

	}

	public double getDanoRecibidoRestante() {
		return danoRecibidoRestante;
	}

	public void agregarUnidades(int cantidad) {
		cantidadDeSoldados += cantidad;
	}

	public double getCantidad() {
		return cantidadDeSoldados;
	}
	public int getCosto() {
		return costo;
	}
	public int getOrden() {
		return orden;
	}
	

	public double getDanoRecibido() {
		return danoRecibido;
	}

	public double getVida() {
		vida = 100 * cantidadDeSoldados;
		return vida;
	}

	public void agregar(Ejercito tipoDeSoldado, int cantidad) {
		
	}
	public ArrayList<Ejercito> getSoldadosSueltos() {
		return soldadosSueltos;
	}

}
