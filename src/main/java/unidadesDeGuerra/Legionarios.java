package unidadesDeGuerra;

public class Legionarios extends Soldado{
	
	private double cantidadDeLegionarios = 1;
	public Legionarios(int cantidadDeLegionarios) {
		super(cantidadDeLegionarios);
		this.cantidadDeLegionarios += cantidadDeLegionarios;
		costo = 100;
		orden = 2;
		nombre = "Legionarios";
		arma = "Espada corta y Escudo";
	}
	@Override
	public void recibirDano(double dano) {
		double cantidadDeMuertos  = dano / 100;
		cantidadDeLegionarios -= cantidadDeMuertos;
		vida -= dano;
		if (vida < 0) {
			danoRecibidoRestante = danoRecibidoRestante - vida;
			vida = 0;
		}
}
	@Override
	public double getVida() {
		vida = 100*cantidadDeLegionarios;
		return vida;
	}
	@Override
	public double getDano() {
		
		dano = (1.4*cantidadDeLegionarios) -1.4;
		
		return dano;
	}
	@Override
	public void agregarUnidades(int cantidad) {
		cantidadDeLegionarios+= cantidad;
	}
	public double getDanoRecibidoRestante() {
		return danoRecibidoRestante;
	}


}
