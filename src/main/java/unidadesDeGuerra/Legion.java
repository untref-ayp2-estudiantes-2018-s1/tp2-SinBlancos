package unidadesDeGuerra;

import java.util.ListIterator;

public class Legion extends Ejercito {

	private Ejercito legionDeLegion;
	private double danoRecibido;
	private double potenciador;

	public Legion(int cantidadDeAuxiliares, int cantidadDeLegionarios, int cantidadDeCenturiones) {
		super(cantidadDeAuxiliares + cantidadDeLegionarios + cantidadDeCenturiones);
		getSoldadosSueltos().add(new Auxiliares(cantidadDeAuxiliares));
		getSoldadosSueltos().add(new Legionarios(cantidadDeLegionarios));
		getSoldadosSueltos().add(new Centuriones(cantidadDeCenturiones));

	}


	void agregarLegion(int cantidadAuxiliares, int cantidadLegionarios, int canidadCenturiones) {
		legionDeLegion = new Legion(cantidadAuxiliares, cantidadLegionarios, canidadCenturiones);

	}

	public Ejercito getLegionDeLegion() {
		return legionDeLegion;
	}

	@Override
	public void recibirDano(double dano) {
		danoRecibido = 0;
		ListIterator<Ejercito> lt = getSoldadosSueltos().listIterator();
		while (lt.hasNext() && getVida() > 0 && dano > 0) {
			Ejercito actual = lt.next();
			if (actual.getOrden() == 1 && actual.getVida() > 0 && dano > 0) {
				actual.recibirDano(dano);
				danoRecibido = actual.getDanoRecibido();
				dano = actual.getDanoRecibidoRestante();
			}
			if (actual.getOrden() == 2 && actual.getVida() > 0 && dano > 0) {
				actual.recibirDano(dano);
				danoRecibido = dano;
				dano = actual.getDanoRecibidoRestante();

			}
			if (actual.getOrden() == 3 && actual.getVida() > 0 && dano > 0) {
				actual.recibirDano(dano);
				danoRecibido = actual.getDanoRecibido();
				dano = actual.getDanoRecibidoRestante();
			}
		}
		if (dano > 0 && legionDeLegion != null){
			legionDeLegion.recibirDano(dano);
		}
	}

	@Override
	public double getVida() {
		vida = 0;
		ListIterator<Ejercito> lt = getSoldadosSueltos().listIterator();
		
		while (lt.hasNext() && getSoldadosSueltos() != null) {
			Ejercito soldadoAcutual = lt.next();
			vida += soldadoAcutual.getVida();
		}
		if (legionDeLegion != null) {
			vida += legionDeLegion.getVida();
		}
		return vida;
	}

	@Override
	public double getDano() {
		dano = 0;
		ListIterator<Ejercito> lt = getSoldadosSueltos().listIterator();
		while (lt.hasNext() && getSoldadosSueltos() != null) {
			Ejercito soldadoAcutual = lt.next();
				dano += soldadoAcutual.getDano();
			
		}
		if(getSoldadosSueltos().get(2).getCantidad() > 1) {
			potenciador = 0.10 * (getSoldadosSueltos().get(2).getCantidad()-1);
			dano += (dano * potenciador);
		}
		if (legionDeLegion != null) {
			dano += legionDeLegion.getDano();
		}
		return dano;
	}

	@Override
	public double getDanoRecibido() {
		return danoRecibido;
	}
	
	@Override
	public void agregar(Ejercito tipoDeSoldado, int cantidad) {
		if(tipoDeSoldado == getSoldadosSueltos().get(0)) {
			getSoldadosSueltos().get(0).agregarUnidades(cantidad);
		}
		if(tipoDeSoldado == getSoldadosSueltos().get(1)) {
			getSoldadosSueltos().get(1).agregarUnidades(cantidad);
		}
		if(tipoDeSoldado == getSoldadosSueltos().get(2)) {
			getSoldadosSueltos().get(2).agregarUnidades(cantidad);
		}
	}

}
