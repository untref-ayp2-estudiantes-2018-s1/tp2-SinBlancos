package Excepciones;

public class ErrorLegionNoExiste extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1626988562762988975L;

	public ErrorLegionNoExiste() {
		super("Esa legion no existe");
		
	}

}
