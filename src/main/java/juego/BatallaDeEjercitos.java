package juego;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.StringTokenizer;

import Excepciones.ErrorLegionNoExiste;

import unidadesDeGuerra.Legionarios;
import unidadesDeGuerra.Soldado;
import unidadesDeGuerra.Auxiliares;
import unidadesDeGuerra.Centuriones;

public class BatallaDeEjercitos {

	private Jugador jugador1;
	private Jugador jugador2;
	private String nombreDeJugador1;
	private String nombreDeJugador2;
	private Jugador primerJugador;
	private String rutaPuntoComa = "/src/main/legionesPC.txt";
	private String rutaComa = "/src/main/legionesC.txt";
	private boolean turnoj1 = false;
	private Jugador ganador;
	private boolean compraInicial;

	public BatallaDeEjercitos(String nombreDeJugador1, String nombreDeJugador2) {

		this.nombreDeJugador1 = nombreDeJugador1;
		this.nombreDeJugador2 = nombreDeJugador2;

		jugador1 = new Jugador(nombreDeJugador1);
		jugador2 = new Jugador(nombreDeJugador2);

		System.out.println("Se creo una nueva batalla");

		System.out.println("El jugador 1 es: " + nombreDeJugador1);
		System.out.println("El jugador 2 es: " + nombreDeJugador2);

	}

	public Jugador getJugador1() {
		return jugador1;
	}

	public Jugador getJugador2() {
		return jugador2;
	}

	public String getNombreDeJugador1() {
		return nombreDeJugador1;
	}

	public String getNombreDeJugador2() {
		return nombreDeJugador2;
	}

	public Jugador getPrimerJugador() {
		return primerJugador;
	}

	public void terminarTurno() {
		int turno = 0;
		if (turnoj1 == false && primerJugador == jugador2 && turno == 0) {
			turnoj1 = true;
			primerJugador = jugador1;
			turno++;

		}
		if (turnoj1 == true && primerJugador == jugador1 && turno == 0) {
			turnoj1 = false;
			primerJugador = jugador2;
			turno++;
		}
	}

	public void terminarPeriodoDeCompraInicial() {
		if (primerJugador == jugador1) {
			turnoj1 = false;
		}
		turnoj1 = true;
		compraInicial = true;
	}

	public Jugador getJugadorActual() {

		if (turnoj1 == true) {
			return jugador1;
		}
		return jugador2;
	}

	public Jugador primerTurnoPorDado() {

		System.out.println("Se tiraron los dados");
		int dadoJugador1;
		int dadoJugador2;
		dadoJugador1 = jugador1.lanzarDado();
		dadoJugador2 = jugador2.lanzarDado();

		if (dadoJugador1 > dadoJugador2) {

			System.out.println(nombreDeJugador1 + " saco el numero " + dadoJugador1);
			System.out.println(nombreDeJugador2 + " saco el numero " + dadoJugador2);

			System.out.println("Comienza el jugador 1 : " + nombreDeJugador1);
			primerJugador = jugador1;
			turnoj1 = true;

		} else if (dadoJugador1 < dadoJugador2) {
			System.out.println(nombreDeJugador1 + " saco el numero " + dadoJugador1);
			System.out.println(nombreDeJugador2 + " saco el numero " + dadoJugador2);

			System.out.println("Comienza el jugador 2 : " + nombreDeJugador2);
			primerJugador = jugador2;
		} else if (dadoJugador1 == dadoJugador2) {
			System.out.println(nombreDeJugador1 + " saco el numero " + dadoJugador1);
			System.out.println(nombreDeJugador2 + " saco el numero " + dadoJugador2);

			System.out.println(nombreDeJugador1 + " y " + nombreDeJugador2 + " tienen el mismo numero.");
			System.out.println("Se tiraron los dados nuevamente");

			primerTurnoPorDado();

		}
		if (primerJugador == jugador1) {
			turnoj1 = true;
		}
		if (primerJugador == jugador2) {
			turnoj1 = false;
		}

		return primerJugador;

	}

	public void crearSoldados(Soldado tipoDeSoldado, int cantidad) {
		if (primerJugador == jugador1) {
			jugador1.agregarSoldados(tipoDeSoldado, cantidad);
			if (jugador1.getDinero() == 0) {
				terminarTurno();
			}

		} else if (primerJugador == jugador2) {
			jugador2.agregarSoldados(tipoDeSoldado, cantidad);
			if (jugador2.getDinero() == 0) {
				terminarTurno();
			}
		}

	}

	public void crearLegion(int auxiliares, int legionarios, int centuriones) {

		if (primerJugador == jugador1) {
			jugador1.crearLegion(auxiliares, legionarios, centuriones);
		} else if (primerJugador == jugador2) {
			jugador2.crearLegion(auxiliares, legionarios, centuriones);		
		}
	}

	public void ataque() {

		int turno = 0;
		if (turnoj1 == true && turno == 0 && !hayGanador()) {
			jugador2.reciboAtaque(jugador1.getDano());
			turnoj1 = false;
			turno++;
			if (jugador1.getLegion().getSoldadosSueltos().get(0).getCantidad() > 1) {
				System.out.println((int) jugador1.getLegion().getSoldadosSueltos().get(0).getFallos() - 1
						+ " auxiliares han fallado!");
			}
			System.out
					.println(jugador2.getNombre() + " recibio " + jugador2.getLegion().getDanoRecibido() + " de dano");
			if (jugador2.getVida() == 0) {
				System.out.println("Al " + jugador2.getNombre() + " ya no le queda queda vida");
			} else {
				System.out.println("A " + jugador2.getNombre() + " le queda " + jugador2.getVida() + " de vida");
			}
		}
		if (turnoj1 == false && turno == 0 && !hayGanador()) {
			jugador1.reciboAtaque(jugador2.getDano());
			turnoj1 = true;
			turno++;
			if (jugador2.getLegion().getSoldadosSueltos().get(0).getCantidad() > 1) {
				System.out.println((int) jugador2.getLegion().getSoldadosSueltos().get(0).getFallos() - 1
						+ " auxiliares han fallado!");
			}
			System.out
					.println(jugador1.getNombre() + " recibio " + jugador1.getLegion().getDanoRecibido() + " de dano");
			if (jugador2.getVida() == 0) {
				System.out.println("Al " + jugador2.getNombre() + " ya no le queda queda vida");
			} else {
				System.out.println("A " + jugador1.getNombre() + " le queda " + jugador1.getVida() + " de vida");
			}
		}

	}

	public boolean hayGanador() {
		boolean resultado = false;
		if ((jugador1.getVida() <= 0 || jugador2.getVida() <= 0) && compraInicial==true) {
			resultado = true;
		}
		return resultado;
	}

	public Jugador ganador() {

		if (jugador1.getVida() < 0) {
			ganador = jugador2;
		}
		if (jugador2.getVida() < 0) {
			ganador = jugador1;
		}
		return ganador;
	}
	public void verLegionesPredefinidasConComa() throws IOException {
		int numero = 1;
		String nombreLegion;
		int auxiliares = 0;
		int legionarios = 0;
		int centuriones = 0;
		String legiones;
		String rutaDeAcceso = new File("").getAbsolutePath();
		rutaDeAcceso += rutaComa;
		FileReader archivo = new FileReader(rutaDeAcceso);
		BufferedReader lector = new BufferedReader(archivo);
		legiones = lector.readLine();
		while (legiones != null) {
			StringTokenizer divisor = new StringTokenizer(legiones, ",");
			nombreLegion = divisor.nextToken();
			auxiliares = Integer.parseInt(divisor.nextToken());
			legionarios = Integer.parseInt(divisor.nextToken());
			centuriones = Integer.parseInt(divisor.nextToken());
			System.out.println(numero + ". " + nombreLegion + " contiene: " + auxiliares + " auxiliares " + ", "
					+ legionarios + " legionarios " + ", " + centuriones + " centuriones");
			legiones = lector.readLine();
			numero++;
		}
	}
	public void verLegionesPredefinidasConPuntoYComa() throws IOException {
		int numero = 8;
		String nombreLegion;
		int auxiliares = 0;
		int legionarios = 0;
		int centuriones = 0;
		String legiones;
		String rutaDeAcceso = new File("").getAbsolutePath();
		rutaDeAcceso += rutaPuntoComa;
		FileReader archivo = new FileReader(rutaDeAcceso);
		BufferedReader lector = new BufferedReader(archivo);
		legiones = lector.readLine();
		while (legiones != null) {
			StringTokenizer divisor = new StringTokenizer(legiones, ";");
			nombreLegion = divisor.nextToken();
			auxiliares = Integer.parseInt(divisor.nextToken());
			legionarios = Integer.parseInt(divisor.nextToken());
			centuriones = Integer.parseInt(divisor.nextToken());
			System.out.println(numero + ". " + nombreLegion + " contiene: " + auxiliares + " auxiliares " + ", "
					+ legionarios + " legionarios " + ", " + centuriones + " centuriones");
			legiones = lector.readLine();
			numero++;
		}
	}

	public void elegirLegionPredefinidaConComa(int legion) throws IOException {
		int numero = 0;
		int auxiliares = 0;
		int legionarios = 0;
		int centuriones = 0;
		String legiones;
		String rutaDeAcceso = new File("").getAbsolutePath();
		rutaDeAcceso += rutaComa;
		FileReader archivo = new FileReader(rutaDeAcceso);
		BufferedReader lector = new BufferedReader(archivo);
		legiones = lector.readLine();
		try {
			if (legion < 0) {
				lector.close();
				throw new ErrorLegionNoExiste();
			} else {

				while (numero != legion) {
					numero++;
					StringTokenizer divisor = new StringTokenizer(legiones, ",");
					if (numero == legion) {
						divisor.nextToken();
						auxiliares = Integer.parseInt(divisor.nextToken());
						legionarios = Integer.parseInt(divisor.nextToken());
						centuriones = Integer.parseInt(divisor.nextToken());
						crearLegion(auxiliares, legionarios, centuriones);

					}
					legiones = lector.readLine();
				}

			}
		}

		catch (ErrorLegionNoExiste a) {
			System.out.println(a);
		}
		lector.close();
		archivo.close();

	}
	public void elegirLegionPredefinidaConPuntoYComa(int legion) throws IOException {
		
		int numero = 7;
		int auxiliares = 0;
		int legionarios = 0;
		int centuriones = 0;
		String legiones;
		String rutaDeAcceso = new File("").getAbsolutePath();
		rutaDeAcceso += rutaPuntoComa;
		FileReader archivo = new FileReader(rutaDeAcceso);
		BufferedReader lector = new BufferedReader(archivo);
		legiones = lector.readLine();
		try {
			if (legion < 0) {
				lector.close();
				throw new ErrorLegionNoExiste();
			} else {
				
				while (numero != legion) {
					numero++;
					StringTokenizer divisor = new StringTokenizer(legiones, ";");
					
					if (numero == legion) {
						divisor.nextToken();
						auxiliares = Integer.parseInt(divisor.nextToken());
						legionarios = Integer.parseInt(divisor.nextToken());
						centuriones = Integer.parseInt(divisor.nextToken());
						crearLegion(auxiliares, legionarios, centuriones);
						
					}
					legiones = lector.readLine();
				}
				
			}
		}
		
		catch (ErrorLegionNoExiste a) {
			System.out.println(a);
		}
		lector.close();
		
	}


	@SuppressWarnings("resource")
	public void menuDeCompraConsola() throws IOException {

		boolean terminarCompra = false;
		while (terminarCompra == false && (getJugadorActual().getDinero() >= 50)) {
			if (compraInicial == false) {
				System.out.println("Turno de " + getJugadorActual().getNombre());
			}
			int opcion;
			System.out.println("Dinero disponible = " + getJugadorActual().getDinero() + ", Comprar Ejercito: ");
			System.out.println("1. Auxiliares");
			System.out.println("2. Legionarios");
			System.out.println("3. Centuriones");
			System.out.println("4. Legiones predefinidas");
			System.out.println("5. Legiones personalizadas");
			System.out.println("6. Terminar compra");
			Scanner scanner = new Scanner(System.in);
			try {
				opcion = scanner.nextInt();
				if (opcion == 1) {
					System.out.println("Elegir cantidad de Auxiliares");
					int cantidad = scanner.nextInt();
					scanner.nextLine();
					Soldado auxiliar = new Auxiliares(0);
					crearSoldados(auxiliar, cantidad);
				}
				if (opcion == 2) {
					System.out.println("Elegir cantidad de Legionarios");
					int cantidad = scanner.nextInt();
					scanner.nextLine();
					Soldado legionario = new Legionarios(0);
					crearSoldados(legionario, cantidad);
				}
				if (opcion == 3) {
					System.out.println("Elegir cantidad de Centuriones");
					int cantidad = scanner.nextInt();
					scanner.nextLine();
					Soldado centuriones = new Centuriones(0);
					crearSoldados(centuriones, cantidad);
				}
				if (opcion == 4) {
					verLegionesPredefinidasConComa();
					verLegionesPredefinidasConPuntoYComa();
					int cantidad = scanner.nextInt();
					scanner.nextLine();
					if(cantidad<8) {
					elegirLegionPredefinidaConComa(cantidad);
					}
					else {
						elegirLegionPredefinidaConPuntoYComa(cantidad);
					}
				}
				if (opcion == 5) {

					System.out.println("Elegir cantidad de auxiliares");
					int cantidadAuxiliares = scanner.nextInt();
					scanner.nextLine();
					System.out.println("Elegir cantidad de legionarios");
					int cantidadLegionarios = scanner.nextInt();
					scanner.nextLine();
					System.out.println("Elegir cantidad de centuriones");
					int cantidadCenturiones = scanner.nextInt();
					scanner.nextLine();
					crearLegion(cantidadAuxiliares, cantidadLegionarios, cantidadCenturiones);
				}

				if (opcion == 6) {
					terminarTurno();
					terminarCompra = true;
				}
			} catch (InputMismatchException e) {
				System.out.println("Debes insertar un numero entero entre 1 y 6");
				scanner.next();
			}
		}
	}

}
