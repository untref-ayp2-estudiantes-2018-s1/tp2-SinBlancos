package juego;

import java.util.Random;

public class Dado {
	
	private static Dado dado;
	
	private Dado() {
		
	}
	
	 static public  Dado obtenerDado() {
		if(dado == null) {
			dado = new Dado();
		}
		return dado;
		
	}
	
	public int lanzarDado() {
		
		Random r = new Random();
		int Low = 1;
		int High = 6;
		int result = r.nextInt(High-Low) + Low;
		
		return result;
		
		
		
	}
	

public static void main (String [] str) {

		Dado dado = Dado.obtenerDado();

		System.out.println(dado.lanzarDado());

	}

}
