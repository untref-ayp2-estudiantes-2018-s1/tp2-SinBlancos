package juego;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import juego.BatallaDeEjercitos;

public class BattleGame {

	public BattleGame() throws Exception {

		System.out.println("Introduzca nombre de jugador 1");
		BufferedReader j1 = new BufferedReader(new InputStreamReader(System.in));
		String jugador1 = j1.readLine();

		System.out.println("Introduzca nombre de jugador 2");
		BufferedReader j2 = new BufferedReader(new InputStreamReader(System.in));
		String jugador2 = j2.readLine();

		BatallaDeEjercitos batalla = new BatallaDeEjercitos(jugador1, jugador2);

		batalla.primerTurnoPorDado();
		batalla.menuDeCompraConsola();
		batalla.menuDeCompraConsola();
		System.out.println("El ejercito de " + batalla.getNombreDeJugador1() + " tiene "
				+ batalla.getJugador1().getVida() + " de vida ");
		System.out.println("El ejercito de " + batalla.getNombreDeJugador2() + " tiene "
				+ batalla.getJugador2().getVida() + " de vida ");
		batalla.terminarPeriodoDeCompraInicial();

		while (batalla.hayGanador() == false) {
			System.out.println("Turno de " + batalla.getJugadorActual().getNombre());
			System.out.println("1. Atacar");
			if (batalla.getJugadorActual().getDinero() >= 50) {
				System.out.println("2. Comprar mas unidades");
			}

			BufferedReader num = new BufferedReader(new InputStreamReader(System.in));
			String numElegido = num.readLine();
			if (numElegido.contains("1")) {
				batalla.ataque();
			}
			if (numElegido.contains("2")) {
				batalla.menuDeCompraConsola();
			}

			if (batalla.hayGanador() == true) {
				System.out.println("El ganador es " + batalla.ganador().getNombre());

			}
		}

	}

}
