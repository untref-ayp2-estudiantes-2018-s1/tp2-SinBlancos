package juego;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.StringTokenizer;
import Excepciones.ErrorLegionNoExiste;
import unidadesDeGuerra.Auxiliares;
import unidadesDeGuerra.Centuriones;
import unidadesDeGuerra.Legion;
import unidadesDeGuerra.Legionarios;

import unidadesDeGuerra.Ejercito;

public class Jugador {

	private Ejercito miLegion;
	private String nombre;
	private double vida;
	private double dano;
	private int dinero = 500000;
	int cantidadDeCenturiones = 0;
	double potenciador = 0;
	int numeroDeAuxiliaresLegion = 0;
	int numeroDeCenturionesLegion = 0;
	int numeroDeLegionariosLegion = 0;

	private Dado dado = Dado.obtenerDado();
	private String rutaPuntoComa = "/src/main/legionesPC.txt";
	private String rutaComa = "/src/main/legionesC.txt";

	public Jugador(String nombre) {
		this.nombre = nombre;

		if (miLegion != null) {
			this.dano = miLegion.getDano();
			actualizarVida();
		}

	}

	public void actualizarDano() {
		dano = 0;
		if (miLegion != null) {
			this.dano = miLegion.getDano();
		}
	}

	public void actualizarVida() {
		if (miLegion != null) {
			this.vida = miLegion.getVida() - 200;
		}
	}

	public String getNombre() {
		return nombre;

	}

	public void agregarSoldados(Ejercito tipoDeSoldado, int cantidad) {
		int costoTotal = cantidad * tipoDeSoldado.getCosto();
		if (costoTotal == 0) {
			System.out.println("Elija un numero mayor a 0");
		}
		if (costoTotal > 0 && dinero > 0) {
			if (costoTotal <= dinero) {
				if (miLegion == null) {
					if (tipoDeSoldado.getOrden() == 1) {
						miLegion = new Legion(cantidad, 0, 0);
						System.out.println("Se crearon " + cantidad + " Auxiliares");
						calcularYMostrarDinero(cantidad, 0);
						numeroDeAuxiliaresLegion += cantidad;
						actualizarDano();
					}
					if (tipoDeSoldado.getOrden() == 2) {
						miLegion = new Legion(0, cantidad, 0);
						System.out.println("Se crearon " + cantidad + " Legionarios");
						calcularYMostrarDinero(cantidad, 1);
						numeroDeLegionariosLegion += cantidad;
						actualizarDano();
					}
					if (tipoDeSoldado.getOrden() == 3) {
						miLegion = new Legion(0, 0, cantidad);
						System.out.println("Se crearon " + cantidad + " Centuriones");
						calcularYMostrarDinero(cantidad, 2);
						numeroDeCenturionesLegion += cantidad;
						actualizarDano();
					}
				} else {
					miLegion.agregar(tipoDeSoldado, cantidad);
					System.out.println("Se crearon " + cantidad + tipoDeSoldado.getNombre());
					if (tipoDeSoldado.getOrden() == 1) {
						calcularYMostrarDinero(cantidad, 0);
						numeroDeAuxiliaresLegion += cantidad;
					}
					if (tipoDeSoldado.getOrden() == 2) {
						calcularYMostrarDinero(cantidad, 1);
						numeroDeLegionariosLegion += cantidad;
					}
					if (tipoDeSoldado.getOrden() == 3) {
						calcularYMostrarDinero(cantidad, 2);
						numeroDeCenturionesLegion += cantidad;
					}
					actualizarDano();
				}

			}else if (costoTotal > dinero) {
				if (miLegion == null) {
					if (tipoDeSoldado.getOrden() == 1) {
						int cantidadMaxima = dinero / 50;
						miLegion = new Legion(cantidadMaxima, 0, 0);
						System.out.println("Se crearon " + cantidadMaxima + " Auxiliares");
						calcularYMostrarDinero(cantidadMaxima, 0);
						numeroDeAuxiliaresLegion += cantidadMaxima;
					}
					if (tipoDeSoldado.getOrden() == 2) {
						int cantidadMaxima = dinero / 100;
						miLegion = new Legion(0, cantidadMaxima, 0);
						System.out.println("Se crearon " + cantidadMaxima + " Legionarios");
						calcularYMostrarDinero(cantidadMaxima, 1);
						numeroDeLegionariosLegion += cantidadMaxima;
					}
					if (tipoDeSoldado.getOrden() == 3) {
						int cantidadMaxima = dinero / 200;
						miLegion = new Legion(0, 0, cantidadMaxima);
						System.out.println("Se crearon " + cantidadMaxima + " Centuriones");
						calcularYMostrarDinero(cantidadMaxima, 2);
						numeroDeCenturionesLegion += cantidadMaxima;
					}
					actualizarDano();
				} else {
					int cantidadMaxima = 0;
					if (tipoDeSoldado.getOrden() == 1) {
						cantidadMaxima = dinero / 50;
						miLegion.agregar(tipoDeSoldado, cantidadMaxima);
						System.out.println("Se crearon " + cantidadMaxima + tipoDeSoldado.getNombre());
						calcularYMostrarDinero(cantidadMaxima, 0);
						numeroDeAuxiliaresLegion += cantidadMaxima;
					}
					if (tipoDeSoldado.getOrden() == 2) {
						cantidadMaxima = dinero / 100;
						miLegion.agregar(tipoDeSoldado, cantidadMaxima);
						System.out.println("Se crearon " + cantidadMaxima + tipoDeSoldado.getNombre());
						calcularYMostrarDinero(cantidadMaxima, 1);
						numeroDeLegionariosLegion += cantidadMaxima;
					}
					if (tipoDeSoldado.getOrden() == 3) {
						cantidadMaxima = dinero / 2;
						miLegion.agregar(tipoDeSoldado, cantidadMaxima);
						System.out.println("Se crearon " + cantidadMaxima + tipoDeSoldado.getNombre());
						calcularYMostrarDinero(cantidadMaxima, 2);
						numeroDeCenturionesLegion += cantidadMaxima;
					}
					actualizarDano();
				}
			}
		}
	}


	private void calcularYMostrarDinero(int cantidad, int index) {
		dinero -= (cantidad * miLegion.getSoldadosSueltos().get(index).getCosto());
	}

	public void crearLegion(int cantidadAuxiliares, int cantidadLegionarios, int cantidadCenturiones) {
		if (miLegion == null) {
			miLegion = new Legion(cantidadAuxiliares,cantidadLegionarios,cantidadCenturiones);
			calcularYMostrarDinero(cantidadAuxiliares, 0);
			calcularYMostrarDinero(cantidadLegionarios, 1);
			calcularYMostrarDinero(cantidadCenturiones, 2);
		}else {
			agregarSoldados(miLegion.getSoldadosSueltos().get(0),cantidadAuxiliares);
			agregarSoldados(miLegion.getSoldadosSueltos().get(1), cantidadLegionarios);
			agregarSoldados(miLegion.getSoldadosSueltos().get(2), cantidadCenturiones);
		}
		System.out.println("Se creo una legion con " + cantidadAuxiliares + " Auxiliares, "
				+ cantidadLegionarios + " Legionarios y " + cantidadCenturiones + " Centuriones.");
	}


	public int lanzarDado() {

		return dado.lanzarDado();
	}
	
	public double getDano() {
		actualizarDano();
		return dano;
	}

	public Auxiliares getAuxiliares() {
		return (Auxiliares) miLegion.getSoldadosSueltos().get(0);
	}

	public Legionarios getLegionarios() {
		return (Legionarios) miLegion.getSoldadosSueltos().get(1);
	}

	public Centuriones getCenturiones() {
		return (Centuriones) miLegion.getSoldadosSueltos().get(2);
	}

	public double getDinero() {
		return dinero;
	}

	public Ejercito getLegion() {
		return miLegion;
	}

	public Dado getDado() {
		return dado;
	}

	public void reciboAtaque(double dano) {
		miLegion.recibirDano(dano);
		actualizarVida();
	}

	public double getVida() {
		actualizarVida();
		return this.vida;
	}

}
