package test;

import org.junit.Assert;
import org.junit.Test;


import unidadesDeGuerra.Ejercito;
import unidadesDeGuerra.Legion;


public class TestLegion {

	@Test
	public void testCrearUnaLegionCantidad() {

		Ejercito legion = new Legion(10, 10, 10);

		double cantidad = legion.getCantidad();
		Assert.assertEquals(31.0, cantidad, 0.01);

	}

	@Test
	public void testVida() {
		Ejercito legion = new Legion(10, 10, 10);

		double vidaActual = legion.getVida();

		Assert.assertEquals(3200.0, vidaActual, 0.01);

	}

	@Test
	public void testDano() {

		Ejercito legion = new Legion(10, 10, 10);

		double dano = legion.getDano();

		Assert.assertEquals(55, dano, 5.0);

	}

	@Test
	public void testRecibirDano() {

		Ejercito legion = new Legion(10, 10, 10);

		legion.recibirDano(333.0);
		double danoRecibido = legion.getDanoRecibido();
		Assert.assertEquals(333.0, danoRecibido, 0.1);

	}
	

	
}
