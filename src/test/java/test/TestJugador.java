package test;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import juego.Jugador;

import unidadesDeGuerra.Auxiliares;
import unidadesDeGuerra.Legionarios;

public class TestJugador {

	@Test
	public void testCrearJugadorAgregaSoldado() throws IOException {

		Jugador jugador = new Jugador("Luis");

		Auxiliares auxiliar = new Auxiliares(30);

		double dinerActual = jugador.getDinero();

		// costo de auxiliar = 50

		Assert.assertEquals(500000, dinerActual, 0.1);

		jugador.agregarSoldados(auxiliar, 30);

		double dinerDespuesDeComprar = jugador.getDinero();

		Assert.assertEquals(498500, dinerDespuesDeComprar, 0.1);

	}

	@Test
	public void testCrearJugadorObtenerVida() throws IOException {

		Jugador jugador = new Jugador("Luis");

		Auxiliares auxiliar = new Auxiliares(30);

		jugador.agregarSoldados(auxiliar, 30);

		double vida = jugador.getVida();

		Assert.assertEquals(3000, vida, 0.1);

	}

	@Test
	public void testCrearJugadoReciboDano() throws IOException {

		Jugador jugador = new Jugador("Luis");

		Auxiliares auxiliar = new Auxiliares(30);

		jugador.agregarSoldados(auxiliar, 30);

		jugador.reciboAtaque(50);
		double vida = jugador.getVida();

		Assert.assertEquals(2950, vida, 0.1);
	}

	@Test
	public void testCrearJugadorDano() throws IOException {

		Jugador jugador = new Jugador("Luis");

		Legionarios legionarios = new Legionarios(30);

		jugador.agregarSoldados(legionarios, 30);

		double dano = jugador.getDano();

		Assert.assertEquals(42, dano, 0.1);

	}

	@Test
	public void testCrearJugadorDinero0() throws IOException {

		Jugador jugador = new Jugador("Luis");

		Auxiliares auxiliar = new Auxiliares(30);

		
		while (jugador.getDinero() > 0) {

			jugador.agregarSoldados(auxiliar, 1);

		}

		Assert.assertEquals(0.0, jugador.getDinero(), 0.1);

	}
	
	
	
	
}
