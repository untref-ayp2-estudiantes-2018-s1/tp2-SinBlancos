package test;

import org.junit.Assert;
import org.junit.Test;

import unidadesDeGuerra.Auxiliares;
import unidadesDeGuerra.Centuriones;
import unidadesDeGuerra.Legionarios;
import unidadesDeGuerra.Soldado;

public class TestSoldado {

	@Test
	public void testAuxiliares() {

		Soldado auxiliares = new Auxiliares(30);

		double cantidad = auxiliares.getCantidad();

		Assert.assertEquals(30, cantidad, 1.0);

	}

	@Test
	public void testAuxiliaresVidaYRecibirDano() {
		Soldado auxiliares = new Auxiliares(30);

		double dano = auxiliares.getDano();

		// El dano es = 0.7 * 30(auxuliares) , maximo error si fallan todos es 21

		Assert.assertEquals(21, dano, 21);

		double vida = auxiliares.getVida();

		Assert.assertEquals(3000.0, vida, 0.1);

		auxiliares.recibirDano(200);

		double vidaDespuesDeRecibirDano = auxiliares.getVida();

		Assert.assertEquals(2800.0, vidaDespuesDeRecibirDano, 0.1);

	}

	@Test
	public void testCenturiones() {

		Soldado centuriones = new Centuriones(30);

		double cantidad = centuriones.getCantidad();

		Assert.assertEquals(30, cantidad, 1.0);

		double dano = centuriones.getDano();

		Assert.assertEquals(30, dano, 1.0);

	}

	@Test
	public void testCenturionesVidaYRecibirDano() {

		Soldado centuriones = new Centuriones(30);

		double vida = centuriones.getVida();

		/*
		 * Por errores de diseno la cantidad De centuriones se inicializa en 1
		 * cantidadDecenturiones = 31 VIDA = 3100
		 */

		Assert.assertEquals(3100.0, vida, 0.1);

		centuriones.recibirDano(200);

		double vidaDespuesDeRecibirDano = centuriones.getVida();

		Assert.assertEquals(2900.0, vidaDespuesDeRecibirDano, 50.0);

	}

	@Test
	public void testLegionarios() {

		Soldado legionarios = new Legionarios(30);

		double cantidad = legionarios.getCantidad();

		Assert.assertEquals(30, cantidad, 1.0);
		double dano = legionarios.getDano();
		// dano = 30*1.4

		Assert.assertEquals(42, dano, 1.0);
	}

	@Test
	public void testLegionariosVidaYRecibirDano() {

		Soldado legionarios = new Legionarios(30);

		double vida = legionarios.getVida();

		/*
		 * Por errores de diseno la cantidad De Legionarios se inicializa en 1
		 * cantidadDeLegionarios = 31 VIDA = 3100
		 * 
		 */

		Assert.assertEquals(3100.0, vida, 0.1);

		legionarios.recibirDano(200);

		double vidaDespuesDeRecibirDano = legionarios.getVida();

		Assert.assertEquals(2900.0, vidaDespuesDeRecibirDano, 0.0);

	}

}
