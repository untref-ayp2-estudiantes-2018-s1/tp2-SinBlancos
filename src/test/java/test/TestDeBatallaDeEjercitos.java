package test;

import org.junit.Assert;
import org.junit.Test;
import juego.BatallaDeEjercitos;
import juego.Jugador;

public class TestDeBatallaDeEjercitos {

	@Test
	public void testHayGanador() {

		BatallaDeEjercitos batalla = new BatallaDeEjercitos("Luis", "Gian");

		Assert.assertFalse(batalla.hayGanador());

	}

	@Test
	public void testJugadoresConNombres() {

		BatallaDeEjercitos batalla = new BatallaDeEjercitos("Luis", "Gian");

		String nombreJugador1 = batalla.getJugador1().getNombre();
		String nombreJugador2 = batalla.getJugador2().getNombre();

		Assert.assertEquals("Luis", nombreJugador1);

		Assert.assertEquals("Gian", nombreJugador2);

	}

	@Test
	public void testTurnoJugadores() {

		BatallaDeEjercitos batalla = new BatallaDeEjercitos("Luis", "Gian");
		Jugador jugador1;
		Jugador jugador2;
		Jugador primerTurno;
		jugador1 = batalla.getJugador1();
		jugador2 = batalla.getJugador1();

		primerTurno = batalla.primerTurnoPorDado();

		if (jugador1 == primerTurno) {

			Assert.assertEquals(jugador1, primerTurno);
			
		} else if (jugador2 == primerTurno) {

			Assert.assertEquals(jugador2, primerTurno);
		}

	}

}
