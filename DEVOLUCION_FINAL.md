# Criterios de Evaluación

Tanto subjetivos como objetivos.


# Subjetivos

Aspectos Actitudinales.

## Organización del Trabajo
  - ¿Cómo se lo vio trabajar al grupo?
## Dedicación y Esfuerzo
  - ¿Fueron capaces de ir más allá de lo estrictamente necesario para aprobar?

Si bien al principio se observó una gran dificultad para un buen trabajo en equipo, una actitud bastante pasiva y aspiraciones mínimas, durante la última semana se observó un aumento de la dedicación y el trabajo. Durante esos días lograron implementar mínimamente el patrón de diseño requerido y demostraron una gran actitud de trabajo, aprendizaje y fortaleza frente a la adversidad inminente.

Debo hacerles notar que vuestro trabajo, a priori, no debería haber sido aprobado. Tiene errores graves impedirían que llegue al nivel de una nota conceptual equivalente a un 4/10 (60% - mínimamente suficiente). La única razón por la que se los ha aprobado ha sido por ese esfuerzo anteriormente mencionado.

Por ello espero que las observaciones que contiene el resto de este documento les sirvan para mejorar en el futuro, así consiguen mejorar las graves faltas que evidencia el trabajo y evitar problemas graves en materias más avanzadas.


# Objetivos

La Aplicación Práctica de Conceptos vistos en la asignatura:

## Análisis y Diseño Orientado a Objetos
  - Uso correcto de Herencia, Composición y Polimorfismo.

  Observaciones:
    - Herencia
      - La clase "Soldado" debería haber sido abstracta. El diseño debería impedir crear instancias de ella.
    - Polimorfismo
      En general, al implementar el patrón Composite no aplicaron correctamente polimorfismo (y, luego tampoco recursión).
      Por ejemplo:
      - Si las cotas de daño de las diferentes subclases de "Soldado" hubieran sido un dato estático de la clase "Soldado", el método "recibirDano" podría haberse hecho completamente polimórfico, evitando que fuera sobrecargado en cada subclase.
      - Si el atributo de las subclases de Soldado "cantidadDe[SubClase]" hubiera sido absorbido con el "cantidadDeUnidades" de Soldado, el patrón podría haberse implementado de forma completa: polimórfica y recursivamente.
      En general, un buen uso de polimorfismo debería implicar el no uso de  conversiones de tipo.

  - Desarrollo Dirigido por Tests (Test-Driven Development).

  Observaciones:
    No usaron TDD (puesto que era opcional, no se les bajó puntos por esto).

## Algoritmia

## Uso Eficiente de Estructuras de Datos
Analizar las fortalezas y debilidades de cada una de las EDs vistas en clase (y de sus distintas implementaciones posibles), y elegir la combinación que resuelva cada componente del problema bajo consideración con la máxima eficiencia posible.

## Uso Correcto de Recursividad

## Uso de Patrones de diseño

Se esperaba que se utilice _Composite_ para los Ejércitos y _Singleton_ para el Menú por Consola.

Observaciones:

  En general, al implementar el patrón _Composite_ no aplicaron correctamente polimorfismo (y, luego tampoco recursión).

  El ArrayList<Ejercito> "soldadosSueltos" en la clase "Ejercito" podría haberse evitado. Esto hubiera diferenciado mejor los casos de (digamos)
  a) un ejército compuesto por N soldados sueltos del tipo X + una legión con M soldados del tipo X, del
  b) un ejército compuesto por N + M soldados del tipo X (sin importar si eran sueltos o no)
  Con la implementación actual, es posible "aplanar" el Composite. Algo que le quita la razón de ser al uso del patrón en sí mismo... (lease: el acceder a sus componentes y procesarlos independientemente de su naturaleza y permitir hacerlo de forma recursiva).

  No se implementó el patrón _Singleton_ para el menú.

  Hubiera sido bueno si se hubiera separado claramente las responsabilidades de:
  - la lógica del juego (una clase "BatallaDeEjercitos", en vez de "Jugador + BatallaDeEjercitos")
  - el papel del jugador dentro del juego (una clase "Jugador", más acotada que la actual)
  - la interfaz del juego (una clase "Menú", en vez de "Jugador + BatallaDeEjercitos + BattleGame")

## Aspectos básicos de Calidad Interna del Código

  - Uso correcto de las Estructuras de Control provistas por el lenguaje.

    Observaciones:
      - En "Jugador.agregarSoldados", hubiera sido preferible el evitar tantos "ifs" en favor de "switch + case"

  - Organización General y Modularidad; Cohesión y Acoplamiento
    Observaciones:
      En general, están bien (salvo los errores de implementación de los patrones).
